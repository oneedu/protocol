// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.23.0
// 	protoc        v3.14.0
// source: user/center/list_area.proto

package center

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

// 区域列表请求
type ListAreaReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 省编号
	// @inject_tag: query:"provinceId"
	ProvinceId int64 `protobuf:"varint,10,opt,name=provinceId,proto3" json:"provinceId,omitempty" query:"provinceId"`
	// 城市编号
	// @inject_tag: query:"cityId"
	CityId int64 `protobuf:"varint,20,opt,name=cityId,proto3" json:"cityId,omitempty" query:"cityId"`
}

func (x *ListAreaReq) Reset() {
	*x = ListAreaReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_list_area_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListAreaReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListAreaReq) ProtoMessage() {}

func (x *ListAreaReq) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_list_area_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListAreaReq.ProtoReflect.Descriptor instead.
func (*ListAreaReq) Descriptor() ([]byte, []int) {
	return file_user_center_list_area_proto_rawDescGZIP(), []int{0}
}

func (x *ListAreaReq) GetProvinceId() int64 {
	if x != nil {
		return x.ProvinceId
	}
	return 0
}

func (x *ListAreaReq) GetCityId() int64 {
	if x != nil {
		return x.CityId
	}
	return 0
}

// 区域列表响应
type ListAreaRsp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 响应码
	// @inject_tag: json:"result"
	Result int32 `protobuf:"varint,10,opt,name=result,proto3" json:"result"`
	// 返回信息
	// @inject_tag: json:"desc"
	Desc string `protobuf:"bytes,20,opt,name=desc,proto3" json:"desc"`
	// 响应内容
	// @inject_tag: json:"body"
	Body *ListAreaBody `protobuf:"bytes,30,opt,name=body,proto3" json:"body"`
}

func (x *ListAreaRsp) Reset() {
	*x = ListAreaRsp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_list_area_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListAreaRsp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListAreaRsp) ProtoMessage() {}

func (x *ListAreaRsp) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_list_area_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListAreaRsp.ProtoReflect.Descriptor instead.
func (*ListAreaRsp) Descriptor() ([]byte, []int) {
	return file_user_center_list_area_proto_rawDescGZIP(), []int{1}
}

func (x *ListAreaRsp) GetResult() int32 {
	if x != nil {
		return x.Result
	}
	return 0
}

func (x *ListAreaRsp) GetDesc() string {
	if x != nil {
		return x.Desc
	}
	return ""
}

func (x *ListAreaRsp) GetBody() *ListAreaBody {
	if x != nil {
		return x.Body
	}
	return nil
}

type ListAreaBody struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 区域列表
	// @inject_tag: json:"areaList"
	AreaList []*AreaList `protobuf:"bytes,10,rep,name=areaList,proto3" json:"areaList"`
}

func (x *ListAreaBody) Reset() {
	*x = ListAreaBody{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_list_area_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListAreaBody) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListAreaBody) ProtoMessage() {}

func (x *ListAreaBody) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_list_area_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListAreaBody.ProtoReflect.Descriptor instead.
func (*ListAreaBody) Descriptor() ([]byte, []int) {
	return file_user_center_list_area_proto_rawDescGZIP(), []int{2}
}

func (x *ListAreaBody) GetAreaList() []*AreaList {
	if x != nil {
		return x.AreaList
	}
	return nil
}

type AreaList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 区域编号
	// @inject_tag: json:"areaId"
	AreaId int64 `protobuf:"varint,10,opt,name=areaId,proto3" json:"areaId"`
	// 区域码
	// @inject_tag: json:"areaCode"
	AreaCode string `protobuf:"bytes,20,opt,name=areaCode,proto3" json:"areaCode"`
	// 区域名字
	// @inject_tag: json:"areaName"
	AreaName string `protobuf:"bytes,30,opt,name=areaName,proto3" json:"areaName"`
}

func (x *AreaList) Reset() {
	*x = AreaList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_list_area_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AreaList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AreaList) ProtoMessage() {}

func (x *AreaList) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_list_area_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AreaList.ProtoReflect.Descriptor instead.
func (*AreaList) Descriptor() ([]byte, []int) {
	return file_user_center_list_area_proto_rawDescGZIP(), []int{3}
}

func (x *AreaList) GetAreaId() int64 {
	if x != nil {
		return x.AreaId
	}
	return 0
}

func (x *AreaList) GetAreaCode() string {
	if x != nil {
		return x.AreaCode
	}
	return ""
}

func (x *AreaList) GetAreaName() string {
	if x != nil {
		return x.AreaName
	}
	return ""
}

var File_user_center_list_area_proto protoreflect.FileDescriptor

var file_user_center_list_area_proto_rawDesc = []byte{
	0x0a, 0x1b, 0x75, 0x73, 0x65, 0x72, 0x2f, 0x63, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x2f, 0x6c, 0x69,
	0x73, 0x74, 0x5f, 0x61, 0x72, 0x65, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0a, 0x75,
	0x73, 0x65, 0x72, 0x43, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x22, 0x45, 0x0a, 0x0b, 0x4c, 0x69, 0x73,
	0x74, 0x41, 0x72, 0x65, 0x61, 0x52, 0x65, 0x71, 0x12, 0x1e, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x76,
	0x69, 0x6e, 0x63, 0x65, 0x49, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a, 0x70, 0x72,
	0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x63, 0x69, 0x74, 0x79,
	0x49, 0x64, 0x18, 0x14, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x63, 0x69, 0x74, 0x79, 0x49, 0x64,
	0x22, 0x67, 0x0a, 0x0b, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x72, 0x65, 0x61, 0x52, 0x73, 0x70, 0x12,
	0x16, 0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x05, 0x52,
	0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x64, 0x65, 0x73, 0x63, 0x18,
	0x14, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x64, 0x65, 0x73, 0x63, 0x12, 0x2c, 0x0a, 0x04, 0x62,
	0x6f, 0x64, 0x79, 0x18, 0x1e, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x18, 0x2e, 0x75, 0x73, 0x65, 0x72,
	0x43, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x72, 0x65, 0x61, 0x42,
	0x6f, 0x64, 0x79, 0x52, 0x04, 0x62, 0x6f, 0x64, 0x79, 0x22, 0x40, 0x0a, 0x0c, 0x4c, 0x69, 0x73,
	0x74, 0x41, 0x72, 0x65, 0x61, 0x42, 0x6f, 0x64, 0x79, 0x12, 0x30, 0x0a, 0x08, 0x61, 0x72, 0x65,
	0x61, 0x4c, 0x69, 0x73, 0x74, 0x18, 0x0a, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x75, 0x73,
	0x65, 0x72, 0x43, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x2e, 0x41, 0x72, 0x65, 0x61, 0x4c, 0x69, 0x73,
	0x74, 0x52, 0x08, 0x61, 0x72, 0x65, 0x61, 0x4c, 0x69, 0x73, 0x74, 0x22, 0x5a, 0x0a, 0x08, 0x41,
	0x72, 0x65, 0x61, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x61, 0x72, 0x65, 0x61, 0x49,
	0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x61, 0x72, 0x65, 0x61, 0x49, 0x64, 0x12,
	0x1a, 0x0a, 0x08, 0x61, 0x72, 0x65, 0x61, 0x43, 0x6f, 0x64, 0x65, 0x18, 0x14, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x61, 0x72, 0x65, 0x61, 0x43, 0x6f, 0x64, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x61,
	0x72, 0x65, 0x61, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x1e, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x61,
	0x72, 0x65, 0x61, 0x4e, 0x61, 0x6d, 0x65, 0x42, 0x0f, 0x5a, 0x0d, 0x63, 0x65, 0x6e, 0x74, 0x65,
	0x72, 0x3b, 0x63, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_user_center_list_area_proto_rawDescOnce sync.Once
	file_user_center_list_area_proto_rawDescData = file_user_center_list_area_proto_rawDesc
)

func file_user_center_list_area_proto_rawDescGZIP() []byte {
	file_user_center_list_area_proto_rawDescOnce.Do(func() {
		file_user_center_list_area_proto_rawDescData = protoimpl.X.CompressGZIP(file_user_center_list_area_proto_rawDescData)
	})
	return file_user_center_list_area_proto_rawDescData
}

var file_user_center_list_area_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_user_center_list_area_proto_goTypes = []interface{}{
	(*ListAreaReq)(nil),  // 0: userCenter.ListAreaReq
	(*ListAreaRsp)(nil),  // 1: userCenter.ListAreaRsp
	(*ListAreaBody)(nil), // 2: userCenter.ListAreaBody
	(*AreaList)(nil),     // 3: userCenter.AreaList
}
var file_user_center_list_area_proto_depIdxs = []int32{
	2, // 0: userCenter.ListAreaRsp.body:type_name -> userCenter.ListAreaBody
	3, // 1: userCenter.ListAreaBody.areaList:type_name -> userCenter.AreaList
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_user_center_list_area_proto_init() }
func file_user_center_list_area_proto_init() {
	if File_user_center_list_area_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_user_center_list_area_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListAreaReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_user_center_list_area_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListAreaRsp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_user_center_list_area_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListAreaBody); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_user_center_list_area_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AreaList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_user_center_list_area_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_user_center_list_area_proto_goTypes,
		DependencyIndexes: file_user_center_list_area_proto_depIdxs,
		MessageInfos:      file_user_center_list_area_proto_msgTypes,
	}.Build()
	File_user_center_list_area_proto = out.File
	file_user_center_list_area_proto_rawDesc = nil
	file_user_center_list_area_proto_goTypes = nil
	file_user_center_list_area_proto_depIdxs = nil
}
