// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.23.0
// 	protoc        v3.14.0
// source: user/center/get_user.proto

package center

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type GetUserReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 用户编号
	// @inject_tag: query:"userId" validate:"required"
	UserId int64 `protobuf:"varint,10,opt,name=userId,proto3" json:"userId,omitempty" query:"userId" validate:"required"`
}

func (x *GetUserReq) Reset() {
	*x = GetUserReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_get_user_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetUserReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetUserReq) ProtoMessage() {}

func (x *GetUserReq) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_get_user_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetUserReq.ProtoReflect.Descriptor instead.
func (*GetUserReq) Descriptor() ([]byte, []int) {
	return file_user_center_get_user_proto_rawDescGZIP(), []int{0}
}

func (x *GetUserReq) GetUserId() int64 {
	if x != nil {
		return x.UserId
	}
	return 0
}

type GetUserRsp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 响应结果
	// @inject_tag: json:"result"
	Result int32 `protobuf:"varint,10,opt,name=result,proto3" json:"result"`
	// 返回信息
	// @inject_tag: json:"desc"
	Desc string `protobuf:"bytes,20,opt,name=desc,proto3" json:"desc"`
	// 响应内容
	// @inject_tag: json:"body"
	Body *GetUserBody `protobuf:"bytes,30,opt,name=body,proto3" json:"body"`
}

func (x *GetUserRsp) Reset() {
	*x = GetUserRsp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_get_user_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetUserRsp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetUserRsp) ProtoMessage() {}

func (x *GetUserRsp) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_get_user_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetUserRsp.ProtoReflect.Descriptor instead.
func (*GetUserRsp) Descriptor() ([]byte, []int) {
	return file_user_center_get_user_proto_rawDescGZIP(), []int{1}
}

func (x *GetUserRsp) GetResult() int32 {
	if x != nil {
		return x.Result
	}
	return 0
}

func (x *GetUserRsp) GetDesc() string {
	if x != nil {
		return x.Desc
	}
	return ""
}

func (x *GetUserRsp) GetBody() *GetUserBody {
	if x != nil {
		return x.Body
	}
	return nil
}

type GetUserBody struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 用户编号
	// @inject_tag: json"userId"
	UserId int64 `protobuf:"varint,10,opt,name=userId,proto3" json:"userId,omitempty"`
	// 昵称
	// @inject_tag: json"nickName"
	NickName string `protobuf:"bytes,20,opt,name=nickName,proto3" json:"nickName,omitempty"`
	// 账户
	// @inject_tag: json"account"
	Account string `protobuf:"bytes,30,opt,name=account,proto3" json:"account,omitempty"`
	// 手机号
	// @inject_tag: json"phone"
	Phone string `protobuf:"bytes,40,opt,name=phone,proto3" json:"phone,omitempty"`
	// 真实姓名
	// @inject_tag: json"trueName"
	TrueName string `protobuf:"bytes,50,opt,name=trueName,proto3" json:"trueName,omitempty"`
	// 性别 1-男 2-女
	// @inject_tag: json"sex"
	Sex int32 `protobuf:"varint,60,opt,name=sex,proto3" json:"sex,omitempty"`
	// 生日
	// @inject_tag: json"birthday"
	Birthday string `protobuf:"bytes,70,opt,name=birthday,proto3" json:"birthday,omitempty"`
	// 邮箱
	// @inject_tag: json"mail"
	Mail string `protobuf:"bytes,80,opt,name=mail,proto3" json:"mail,omitempty"`
	// 头像地址
	// @inject_tag: json"headUrl"
	HeadUrl string `protobuf:"bytes,90,opt,name=headUrl,proto3" json:"headUrl,omitempty"`
	// 积分值
	// @inject_tag: json"integral"
	Integral int64 `protobuf:"varint,100,opt,name=integral,proto3" json:"integral,omitempty"`
	// 省份id
	// @inject_tag: json"provinceId"
	ProvinceId int64 `protobuf:"varint,110,opt,name=provinceId,proto3" json:"provinceId,omitempty"`
	// 省份名字
	// @inject_tag: json"provinceName"
	ProvinceName string `protobuf:"bytes,120,opt,name=provinceName,proto3" json:"provinceName,omitempty"`
	// 用户类型
	// @inject_tag: json"role"
	Role int64 `protobuf:"varint,130,opt,name=role,proto3" json:"role,omitempty"`
	// 平台信息列表
	// @inject_tag: json"platformList"
	PlatformList []*PlatformList `protobuf:"bytes,140,rep,name=platformList,proto3" json:"platformList,omitempty"`
}

func (x *GetUserBody) Reset() {
	*x = GetUserBody{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_get_user_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetUserBody) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetUserBody) ProtoMessage() {}

func (x *GetUserBody) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_get_user_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetUserBody.ProtoReflect.Descriptor instead.
func (*GetUserBody) Descriptor() ([]byte, []int) {
	return file_user_center_get_user_proto_rawDescGZIP(), []int{2}
}

func (x *GetUserBody) GetUserId() int64 {
	if x != nil {
		return x.UserId
	}
	return 0
}

func (x *GetUserBody) GetNickName() string {
	if x != nil {
		return x.NickName
	}
	return ""
}

func (x *GetUserBody) GetAccount() string {
	if x != nil {
		return x.Account
	}
	return ""
}

func (x *GetUserBody) GetPhone() string {
	if x != nil {
		return x.Phone
	}
	return ""
}

func (x *GetUserBody) GetTrueName() string {
	if x != nil {
		return x.TrueName
	}
	return ""
}

func (x *GetUserBody) GetSex() int32 {
	if x != nil {
		return x.Sex
	}
	return 0
}

func (x *GetUserBody) GetBirthday() string {
	if x != nil {
		return x.Birthday
	}
	return ""
}

func (x *GetUserBody) GetMail() string {
	if x != nil {
		return x.Mail
	}
	return ""
}

func (x *GetUserBody) GetHeadUrl() string {
	if x != nil {
		return x.HeadUrl
	}
	return ""
}

func (x *GetUserBody) GetIntegral() int64 {
	if x != nil {
		return x.Integral
	}
	return 0
}

func (x *GetUserBody) GetProvinceId() int64 {
	if x != nil {
		return x.ProvinceId
	}
	return 0
}

func (x *GetUserBody) GetProvinceName() string {
	if x != nil {
		return x.ProvinceName
	}
	return ""
}

func (x *GetUserBody) GetRole() int64 {
	if x != nil {
		return x.Role
	}
	return 0
}

func (x *GetUserBody) GetPlatformList() []*PlatformList {
	if x != nil {
		return x.PlatformList
	}
	return nil
}

type PlatformList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 平台id
	// @inject_tag: json"platformId"
	PlatformId int64 `protobuf:"varint,10,opt,name=platformId,proto3" json:"platformId,omitempty"`
	// 平台用户id
	// @inject_tag: json"platformUserId"
	PlatformUserId string `protobuf:"bytes,20,opt,name=platformUserId,proto3" json:"platformUserId,omitempty"`
	// 角色列表
	// @inject_tag: json"roleArray"
	RoleArray []*Role `protobuf:"bytes,30,rep,name=roleArray,proto3" json:"roleArray,omitempty"`
}

func (x *PlatformList) Reset() {
	*x = PlatformList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_get_user_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PlatformList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PlatformList) ProtoMessage() {}

func (x *PlatformList) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_get_user_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PlatformList.ProtoReflect.Descriptor instead.
func (*PlatformList) Descriptor() ([]byte, []int) {
	return file_user_center_get_user_proto_rawDescGZIP(), []int{3}
}

func (x *PlatformList) GetPlatformId() int64 {
	if x != nil {
		return x.PlatformId
	}
	return 0
}

func (x *PlatformList) GetPlatformUserId() string {
	if x != nil {
		return x.PlatformUserId
	}
	return ""
}

func (x *PlatformList) GetRoleArray() []*Role {
	if x != nil {
		return x.RoleArray
	}
	return nil
}

type Role struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 角色id
	// @inject_tag: json"roleId"
	RoleId int64 `protobuf:"varint,10,opt,name=roleId,proto3" json:"roleId,omitempty"`
	// 班级编号
	// @inject_tag: json"classId"
	ClassId int64 `protobuf:"varint,20,opt,name=classId,proto3" json:"classId,omitempty"`
	// 班级名字
	// @inject_tag: json"className"
	ClassName string `protobuf:"bytes,30,opt,name=className,proto3" json:"className,omitempty"`
	// 年级编号
	// @inject_tag: json"gradeId"
	GradeId int64 `protobuf:"varint,40,opt,name=gradeId,proto3" json:"gradeId,omitempty"`
	// 年级名字
	// @inject_tag: json"gradeName"
	GradeName string `protobuf:"bytes,50,opt,name=gradeName,proto3" json:"gradeName,omitempty"`
	// 学校编号
	// @inject_tag: json"schoolId"
	SchoolId int64 `protobuf:"varint,60,opt,name=schoolId,proto3" json:"schoolId,omitempty"`
	// 学校名字
	// @inject_tag: json"schoolName"
	SchoolName string `protobuf:"bytes,80,opt,name=schoolName,proto3" json:"schoolName,omitempty"`
}

func (x *Role) Reset() {
	*x = Role{}
	if protoimpl.UnsafeEnabled {
		mi := &file_user_center_get_user_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Role) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Role) ProtoMessage() {}

func (x *Role) ProtoReflect() protoreflect.Message {
	mi := &file_user_center_get_user_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Role.ProtoReflect.Descriptor instead.
func (*Role) Descriptor() ([]byte, []int) {
	return file_user_center_get_user_proto_rawDescGZIP(), []int{4}
}

func (x *Role) GetRoleId() int64 {
	if x != nil {
		return x.RoleId
	}
	return 0
}

func (x *Role) GetClassId() int64 {
	if x != nil {
		return x.ClassId
	}
	return 0
}

func (x *Role) GetClassName() string {
	if x != nil {
		return x.ClassName
	}
	return ""
}

func (x *Role) GetGradeId() int64 {
	if x != nil {
		return x.GradeId
	}
	return 0
}

func (x *Role) GetGradeName() string {
	if x != nil {
		return x.GradeName
	}
	return ""
}

func (x *Role) GetSchoolId() int64 {
	if x != nil {
		return x.SchoolId
	}
	return 0
}

func (x *Role) GetSchoolName() string {
	if x != nil {
		return x.SchoolName
	}
	return ""
}

var File_user_center_get_user_proto protoreflect.FileDescriptor

var file_user_center_get_user_proto_rawDesc = []byte{
	0x0a, 0x1a, 0x75, 0x73, 0x65, 0x72, 0x2f, 0x63, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x2f, 0x67, 0x65,
	0x74, 0x5f, 0x75, 0x73, 0x65, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0a, 0x75, 0x73,
	0x65, 0x72, 0x43, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x22, 0x24, 0x0a, 0x0a, 0x47, 0x65, 0x74, 0x55,
	0x73, 0x65, 0x72, 0x52, 0x65, 0x71, 0x12, 0x16, 0x0a, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64,
	0x18, 0x0a, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x22, 0x65,
	0x0a, 0x0a, 0x47, 0x65, 0x74, 0x55, 0x73, 0x65, 0x72, 0x52, 0x73, 0x70, 0x12, 0x16, 0x0a, 0x06,
	0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06, 0x72, 0x65,
	0x73, 0x75, 0x6c, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x64, 0x65, 0x73, 0x63, 0x18, 0x14, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x04, 0x64, 0x65, 0x73, 0x63, 0x12, 0x2b, 0x0a, 0x04, 0x62, 0x6f, 0x64, 0x79,
	0x18, 0x1e, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x75, 0x73, 0x65, 0x72, 0x43, 0x65, 0x6e,
	0x74, 0x65, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x55, 0x73, 0x65, 0x72, 0x42, 0x6f, 0x64, 0x79, 0x52,
	0x04, 0x62, 0x6f, 0x64, 0x79, 0x22, 0x9d, 0x03, 0x0a, 0x0b, 0x47, 0x65, 0x74, 0x55, 0x73, 0x65,
	0x72, 0x42, 0x6f, 0x64, 0x79, 0x12, 0x16, 0x0a, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x18,
	0x0a, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1a, 0x0a,
	0x08, 0x6e, 0x69, 0x63, 0x6b, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x14, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x08, 0x6e, 0x69, 0x63, 0x6b, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x63, 0x63,
	0x6f, 0x75, 0x6e, 0x74, 0x18, 0x1e, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x63, 0x63, 0x6f,
	0x75, 0x6e, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x18, 0x28, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x74, 0x72, 0x75,
	0x65, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x32, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x74, 0x72, 0x75,
	0x65, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x73, 0x65, 0x78, 0x18, 0x3c, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x03, 0x73, 0x65, 0x78, 0x12, 0x1a, 0x0a, 0x08, 0x62, 0x69, 0x72, 0x74, 0x68,
	0x64, 0x61, 0x79, 0x18, 0x46, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x69, 0x72, 0x74, 0x68,
	0x64, 0x61, 0x79, 0x12, 0x12, 0x0a, 0x04, 0x6d, 0x61, 0x69, 0x6c, 0x18, 0x50, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x04, 0x6d, 0x61, 0x69, 0x6c, 0x12, 0x18, 0x0a, 0x07, 0x68, 0x65, 0x61, 0x64, 0x55,
	0x72, 0x6c, 0x18, 0x5a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x68, 0x65, 0x61, 0x64, 0x55, 0x72,
	0x6c, 0x12, 0x1a, 0x0a, 0x08, 0x69, 0x6e, 0x74, 0x65, 0x67, 0x72, 0x61, 0x6c, 0x18, 0x64, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x08, 0x69, 0x6e, 0x74, 0x65, 0x67, 0x72, 0x61, 0x6c, 0x12, 0x1e, 0x0a,
	0x0a, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x49, 0x64, 0x18, 0x6e, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x0a, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x49, 0x64, 0x12, 0x22, 0x0a,
	0x0c, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x78, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0c, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x4e, 0x61, 0x6d,
	0x65, 0x12, 0x13, 0x0a, 0x04, 0x72, 0x6f, 0x6c, 0x65, 0x18, 0x82, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x04, 0x72, 0x6f, 0x6c, 0x65, 0x12, 0x3d, 0x0a, 0x0c, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x4c, 0x69, 0x73, 0x74, 0x18, 0x8c, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x18, 0x2e,
	0x75, 0x73, 0x65, 0x72, 0x43, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x2e, 0x50, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x0c, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x4c, 0x69, 0x73, 0x74, 0x22, 0x86, 0x01, 0x0a, 0x0c, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x49, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x49, 0x64, 0x12, 0x26, 0x0a, 0x0e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x55, 0x73, 0x65, 0x72, 0x49, 0x64, 0x18, 0x14, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e,
	0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x55, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12, 0x2e,
	0x0a, 0x09, 0x72, 0x6f, 0x6c, 0x65, 0x41, 0x72, 0x72, 0x61, 0x79, 0x18, 0x1e, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x10, 0x2e, 0x75, 0x73, 0x65, 0x72, 0x43, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x2e, 0x52,
	0x6f, 0x6c, 0x65, 0x52, 0x09, 0x72, 0x6f, 0x6c, 0x65, 0x41, 0x72, 0x72, 0x61, 0x79, 0x22, 0xca,
	0x01, 0x0a, 0x04, 0x52, 0x6f, 0x6c, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x6f, 0x6c, 0x65, 0x49,
	0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x72, 0x6f, 0x6c, 0x65, 0x49, 0x64, 0x12,
	0x18, 0x0a, 0x07, 0x63, 0x6c, 0x61, 0x73, 0x73, 0x49, 0x64, 0x18, 0x14, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x07, 0x63, 0x6c, 0x61, 0x73, 0x73, 0x49, 0x64, 0x12, 0x1c, 0x0a, 0x09, 0x63, 0x6c, 0x61,
	0x73, 0x73, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x1e, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x6c,
	0x61, 0x73, 0x73, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x67, 0x72, 0x61, 0x64, 0x65,
	0x49, 0x64, 0x18, 0x28, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x67, 0x72, 0x61, 0x64, 0x65, 0x49,
	0x64, 0x12, 0x1c, 0x0a, 0x09, 0x67, 0x72, 0x61, 0x64, 0x65, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x32,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x67, 0x72, 0x61, 0x64, 0x65, 0x4e, 0x61, 0x6d, 0x65, 0x12,
	0x1a, 0x0a, 0x08, 0x73, 0x63, 0x68, 0x6f, 0x6f, 0x6c, 0x49, 0x64, 0x18, 0x3c, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x08, 0x73, 0x63, 0x68, 0x6f, 0x6f, 0x6c, 0x49, 0x64, 0x12, 0x1e, 0x0a, 0x0a, 0x73,
	0x63, 0x68, 0x6f, 0x6f, 0x6c, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x50, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0a, 0x73, 0x63, 0x68, 0x6f, 0x6f, 0x6c, 0x4e, 0x61, 0x6d, 0x65, 0x42, 0x0f, 0x5a, 0x0d, 0x63,
	0x65, 0x6e, 0x74, 0x65, 0x72, 0x3b, 0x63, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_user_center_get_user_proto_rawDescOnce sync.Once
	file_user_center_get_user_proto_rawDescData = file_user_center_get_user_proto_rawDesc
)

func file_user_center_get_user_proto_rawDescGZIP() []byte {
	file_user_center_get_user_proto_rawDescOnce.Do(func() {
		file_user_center_get_user_proto_rawDescData = protoimpl.X.CompressGZIP(file_user_center_get_user_proto_rawDescData)
	})
	return file_user_center_get_user_proto_rawDescData
}

var file_user_center_get_user_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_user_center_get_user_proto_goTypes = []interface{}{
	(*GetUserReq)(nil),   // 0: userCenter.GetUserReq
	(*GetUserRsp)(nil),   // 1: userCenter.GetUserRsp
	(*GetUserBody)(nil),  // 2: userCenter.GetUserBody
	(*PlatformList)(nil), // 3: userCenter.PlatformList
	(*Role)(nil),         // 4: userCenter.Role
}
var file_user_center_get_user_proto_depIdxs = []int32{
	2, // 0: userCenter.GetUserRsp.body:type_name -> userCenter.GetUserBody
	3, // 1: userCenter.GetUserBody.platformList:type_name -> userCenter.PlatformList
	4, // 2: userCenter.PlatformList.roleArray:type_name -> userCenter.Role
	3, // [3:3] is the sub-list for method output_type
	3, // [3:3] is the sub-list for method input_type
	3, // [3:3] is the sub-list for extension type_name
	3, // [3:3] is the sub-list for extension extendee
	0, // [0:3] is the sub-list for field type_name
}

func init() { file_user_center_get_user_proto_init() }
func file_user_center_get_user_proto_init() {
	if File_user_center_get_user_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_user_center_get_user_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetUserReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_user_center_get_user_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetUserRsp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_user_center_get_user_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetUserBody); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_user_center_get_user_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PlatformList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_user_center_get_user_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Role); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_user_center_get_user_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_user_center_get_user_proto_goTypes,
		DependencyIndexes: file_user_center_get_user_proto_depIdxs,
		MessageInfos:      file_user_center_get_user_proto_msgTypes,
	}.Build()
	File_user_center_get_user_proto = out.File
	file_user_center_get_user_proto_rawDesc = nil
	file_user_center_get_user_proto_goTypes = nil
	file_user_center_get_user_proto_depIdxs = nil
}
